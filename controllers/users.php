<?php 
  $app->get('/home/',$has_db($app),function() use($app){
  $pagina = "usuarios/";
  $data = array();
  if ($_SESSION['usuario'] == "caja") {
      $pagina = $pagina."caja.twig";
      $st = $app->db->prepare('SELECT * FROM menu ORDER BY platillo');
      $st->setFetchMode(PDO::FETCH_OBJ);
      $st->execute();
      $menu = $st->fetchAll();
      $st2 = $app->db->prepare('SELECT * FROM mesas ORDER BY numero');
      $st2->setFetchMode(PDO::FETCH_OBJ);
      $st2->execute();
      $mesa = $st2 ->fetchAll();
      $data['menu'] = $menu;
      $data['mesas'] = $mesa;
    }else if($_SESSION['usuario'] == "cocina"){
      $pagina = $pagina."cocina.twig";
    }else if($_SESSION['usuario'] == "admin"){
      $pagina = $pagina."admin.twig";
    }else{
      return redirect($app->urlFor('home'));
    }
      return $app->render($pagina,$data);
})->name('load-user');
 ?>