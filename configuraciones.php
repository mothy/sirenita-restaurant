<?php
session_cache_limiter(false);
session_start();
date_default_timezone_set('America/Mexico_City');
// CONFIG
define('SYS_TITLE', 'Restaurant');
define('SYS_NAME', 'Restaurant');
define('SYS_DEBUG', true);
// Slim Vars
define('COOKIE_PREFIX', 'mothy');
define('COOKIES_ENABLED', true);
define('COOKIE_SECRET', 'r3s5t4uR4nT');
define('COOKIE_DURATION', '60 minutes');
define('COOKIE_NAME', COOKIE_PREFIX.SYS_TITLE);
define('SLIM_MODE','development');//development,production
// FOLDERS
define('ASSETS_DIR', 'html/');
define('CONTROLLERS_DIR', 'controllers/');
define('VIEWS_DIR', 'views/');
define('MODELS_DIR', 'models/');
$app = new \Slim\Slim();
if(COOKIES_ENABLED) {
  $app->add(new \Slim\Middleware\SessionCookie(array(
    'secret'  => md5(COOKIE_SECRET),
    'expires' => COOKIE_DURATION,
    'name'    => COOKIE_NAME
  )));
}
$app->config(array(
  'debug' => true,
  'templates.path' => VIEWS_DIR,
  'view' => new \Slim\Views\Twig(),
  'mode' => SLIM_MODE
));
$app->configureMode('production', function () use ($app) {
  $app->config(array('log.enabled' => true, 'debug' => false));
});
$app->configureMode('development', function () use ($app) {
  $app->config(array('log.enabled' => false, 'debug' => true));
});

$app->notFound(function () use ($app) {
  //$app->render('errors/404.twig');
  echo "error 404";
});
$app->error(function (\Exception $e) use ($app) {
  //$app->render('errors/500.twig');
  echo "error500";
});
$app->session = $_SESSION;
$app->flash = (isset($_SESSION['slim.flash'])) ? $_SESSION['slim.flash'] : null;

$rootUri = $app->request()->getRootUri();
$assetUri = $rootUri;
$resourceUri = $_SERVER['REQUEST_URI'];
$parts = explode("/",$app->request()->getPathInfo());

$view = $app->view();
$app->view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
    new Twig_Extension_Debug()
);
$app->view()->appendData(array(
  'title' => SYS_TITLE,
  'name' => SYS_NAME,
  'app' => $app,
  'root' => $rootUri,
  'home' => "$rootUri/{$parts[1]}/",
  'assetUri' => $assetUri,
  'activeUrl' => $resourceUri,
  'flash' => $app->flash
));

$twig = $app->view()->getEnvironment();
$twig->addFunction(new Twig_SimpleFunction('asset', function ($asset) use($app) {
    $ext = pathinfo($asset, PATHINFO_EXTENSION);
    $base = $app->request()->getRootUri();
    switch ($ext) {
      case 'css':
        $dir = $base.'/web/css';
        break;
      case 'js':
        $dir = $base.'/web/js';
        break;
      case 'jpg':
      case 'png':
      case 'gif':
        $dir = $base.'/web/img';
        break;
      case 'mp4':
        $dir = $base.'/web/video';
        break;
      default:
        $dir = $base;
        break;
    }
    return sprintf("$dir/%s", ltrim($asset, '/'));
}));
// DATABASE
$db = null;
$dbs = array(
  'local' => array(
    'DRIVER' => 'pgsql',
    'PORT' => 5432,
    'HOST' => '127.0.0.1',
    'NAME' => 'restaurant',
    'USER' => 'postgres',
    'PASS' => ''
  )
);
$where = exec("hostname",$ouput,$status);
$id = ($where == 'mothy') ? 'local' : 'local';

try {
  $db = new PDO($dbs[$id]['DRIVER'].":host=".$dbs[$id]['HOST'].";dbname=".$dbs[$id]['NAME'],$dbs[$id]['USER'] ,$dbs[$id]['PASS']);
  $db->exec("SET CHARACTER SET utf8");
} catch (PDOException $e) {
  return $e->getMessage();
}
$app->db = $db;
$app->session = $_SESSION;

foreach(glob(MODELS_DIR.'*.php') as $model) {
  include_once $model;
}
 ?>
