<?php 

$app->post('/admin-view/',function () use($app){
	$value = $_POST['val'];
	$ruta = "Admin/";
	$data =array();
	switch ($value) {
		case 'home':
			$ruta = $ruta."home-admin.twig";
			break;
		case 'menu':
			$st = $app->db->prepare("SELECT * FROM menu ORDER BY id");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$data['all'] = $st->fetchAll();
			$ruta = $ruta."Menu-admin.twig";
			break;
		case 'inventario':
			$st = $app->db->prepare("SELECT id,numero,status,deuda FROM mesas");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$data['all'] = $st->fetchAll();
			$ruta = $ruta."inventario-admin.twig";
			break;
		case 'ventas':
			$fechas = array();
			$ventas = array();
			$ingreso = 0;
			$st = $app->db->prepare("SELECT SUM(ingreso) AS ingreso, fecha FROM ingresos GROUP BY (fecha) ORDER BY fecha desc");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$data['all'] = $st->fetchAll();
			/*
			echo json_encode($data['all']);
			die();
			*/
			$ruta = $ruta."ventas-admin.twig";
			break;
		default:
			echo "error";
			break;
	}
	return $app->render($ruta, $data);
})->name('admin-view');

$app->post('/deshabilitar-menu/',function () use($app){
	$id = $_POST['id'];
	$st = $app->db->prepare("UPDATE menu SET existencia = 'f' WHERE id = ?");
	$st->execute(array($id));
	$st = $app->db->prepare("SELECT * FROM menu ORDER BY id");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute();
	$data['all'] = $st->fetchAll();
	return $app->render("Admin/Menu-admin.twig", $data);
})->name('deshabilitar-menu');
 
$app->post('/habilitar-menu/',function () use($app){
	$id = $_POST['id'];
	$st = $app->db->prepare("UPDATE menu SET existencia = 't' WHERE id = ?");
	$st->execute(array($id));
	$st = $app->db->prepare("SELECT * FROM menu ORDER BY id");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute();
	$data['all'] = $st->fetchAll();
	return $app->render("Admin/Menu-admin.twig", $data);
})->name('habilitar-menu');

$app->post('/eliminar-menu/',function () use($app){
	$id = $_POST['id'];
	$st = $app->db->prepare("DELETE FROM menu WHERE id = ?");
	$st->execute(array($id));
	$st = $app->db->prepare("SELECT * FROM menu ORDER BY id");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute();
	$data['all'] = $st->fetchAll();
	return $app->render("Admin/Menu-admin.twig", $data);
})->name('eliminar-menu');

$app->post('/editar-menu/',function () use($app){
	return $app->render('Admin/edit-menu.twig');
})->name('edit-menu');

$app->post('/registrar-menu/',function () use($app){
	$nombre = $_POST['nombre'];
	$precio = $_POST['precio'];
	if (is_numeric($precio)){
		$st = $app->db->prepare("INSERT INTO menu (platillo, precio, existencia) VALUES(?, ?, 't')");
		$st->execute(array($nombre, $precio));
		$app->redirect($app->urlFor('load-user'));
	}else{
		$app->redirect($app->urlFor('load-user'));
	}
})->name('registrar-menu');

$app->post('/change_menu/',function () use($app){
	$type = $_POST['type'];
	$id = $_POST['id'];
	switch ($type) {
		case 'modal':
			$st = $app->db->prepare("SELECT id,platillo,precio FROM menu WHERE id = ?");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute(array($id));
			$data['all'] = $st->fetch();
			return $app->render('Admin/cambiar-menu.twig',$data);
			break;
		case 'procesar':
			$platillo = $_POST['nombre'];
			$precio = $_POST['precio'];
			$rt = $app->db->prepare("UPDATE menu SET platillo = ?, precio = ? WHERE id = ?");
			$rt->execute(array($platillo, $precio, $id));
			return $app->redirect($app->urlFor('load-user'));
			break;
		default:
			return $app->redirect($app->urlFor('load-user'));
			break;
	}
})->name('modal_menu');

$app->post('/load-detalles/',function () use($app){
	$fecha = $_POST['fecha'];
	return $app->render("Admin/view-detalles.twig");
})->name('load-ingresos');

$app->post('/del-mesa/',function () use($app){
	$id_mesa = $_POST['idm'];
	$st = $app->db->prepare("DELETE FROM mesas WHERE id = ?");
	$st->execute(array($id_mesa));
	return $app->redirect($app->urlFor('load-user'));
})->name('del-mesa');

$app->post('/add-mesa/',function () use($app){
	$st = $app->db->prepare("SELECT numero FROM mesas");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute();
	$mesas = $st->fetchAll();
	$numero = count($mesas) + 1;
	$st = $app->db->prepare("INSERT INTO mesas (numero, status, deuda) VALUES(?, ?, 0)");
	$st->execute(array($numero, 't'));
	return $app->redirect($app->urlFor('load-user'));
})->name('add-mesa');
?>