<?php 

$app->post('/pedido/',function() use($app){
	switch ($_POST['act']) {
		case 'pedir':
			$idp = $_POST['idp'];
			$st = $app->db->prepare('SELECT pedidos FROM carrito');
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$consulta = $st->fetch();
			$pedidos = explode(",",$consulta->pedidos);
			$pedidos[] = $idp;
			$val = implode(",",$pedidos);
			$st2 = $app->db->prepare('UPDATE carrito SET pedidos = ?');
			$st2->execute(array($val));
			break;
		case 'mostrar':
			$st = $app->db->prepare('SELECT pedidos FROM carrito');
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$consulta = $st->fetch();
			$pedidos = explode(",",$consulta->pedidos);
			$data = array();
			foreach ($pedidos as $value) {
				$st = $app->db->prepare('SELECT * FROM menu WHERE id = ?');
				$st->setFetchMode(PDO::FETCH_OBJ);
				$st->execute(array($value));
				$consulta = $st->fetch();
				$data['all'][] = $consulta;
			}
			$es = $app->db->prepare('SELECT * FROM mesas');
			$es->setFetchMode(PDO::FETCH_OBJ);
			$es->execute();
			$mesas = $es->fetchAll();
			$data['mesas'] = $mesas;
			unset($data['all'][0]);
			return $app->render('pedidos/carrito.twig',$data);
			break;
		case 'borrar':
			$idb = $_POST['idp'];
			$st = $app->db->prepare('SELECT pedidos FROM carrito');
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$actual = $st->fetch();
			$actualarr = explode(",",$actual->pedidos);
			/*
			*/
			for ($i=0; $i < count($actualarr); $i++) { 
				if($actualarr[$i] == $idb){
					unset($actualarr[$i]);
					$idb = false;
				}
			}
			$val = implode(",",$actualarr);
			$upd = $app->db->prepare('UPDATE carrito SET pedidos = ?');
			$upd->execute(array($val));
			
			$st = $app->db->prepare('SELECT pedidos FROM carrito');
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute();
			$consulta = $st->fetch();
			$pedidos = explode(",", $consulta->pedidos);
			$data = array();
			foreach ($pedidos as $value) {
				$st = $app->db->prepare('SELECT * FROM menu WHERE id = ?');
				$st->setFetchMode(PDO::FETCH_OBJ);
				$st->execute(array($value));
				$consulta = $st->fetch();
				$data['all'][] = $consulta;
			}
			$es = $app->db->prepare('SELECT * FROM mesas');
			$es->setFetchMode(PDO::FETCH_OBJ);
			$es->execute();
			$mesas = $es->fetchAll();
			$data['mesas'] = $mesas;
			unset($data['all'][0]);
			return $app->render('pedidos/carrito.twig',$data);
			break;
		default:
			echo "error";
			break;
	}
	
})->name('pedir');

$app->post('/ordenar_a_cocina/',function () use($app){
	$id_mesa = ($_POST['mesa'] != "") ? $_POST['mesa'] : null ;
	if (!is_null($id_mesa)) {
		$st = $app->db->prepare("SELECT detalles FROM mesas WHERE id = ?");
		$st->setFetchMode(PDO::FETCH_OBJ);
		$st->execute(array($id_mesa));
		$details = $st->fetch();
		$detalles = ($_POST['detalles'] == "") ? $details->detalles : $_POST['detalles'];
		$st = $app->db->prepare("SELECT deuda, pedidos FROM mesas WHERE id = ?");
		$st->setFetchMode(PDO::FETCH_OBJ);
		$st->execute(array($id_mesa));
		$aux = $st->fetch();
		$deuda = $aux->deuda;
		$st = $app->db->prepare("SELECT pedidos FROM carrito");
		$st->setFetchMode(PDO::FETCH_OBJ);
		$st->execute();
		$consulta = $st->fetch();
		$pedidos = explode(",",$consulta->pedidos);
		$st = $app->db->prepare("UPDATE carrito SET mesa = 0, pedidos = ''");
		$st->execute();
		$compra = array();
		unset($pedidos[0]);
		$idp = array();
		foreach ($pedidos as $id) {
			$st = $app->db->prepare("SELECT precio, id FROM menu WHERE id = ?");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute(array($id));
			$precio = $st->fetch();
			$deuda = $deuda + $precio->precio;
			$st = $app->db->prepare("UPDATE mesas SET deuda = ?, status = 'f'  WHERE id = ?");
			$st->execute(array($deuda, $id_mesa));
			$idp[] = $precio->id;	
		}
		$ids =$aux->pedidos.",".implode(",", $idp);
		$st = $app->db->prepare("UPDATE mesas SET pedidos = ?, detalles = ?, hora = now() WHERE id = ?");
		$st->execute(array($ids, $detalles, $id_mesa));
		$app->flash('message', 'se pidio la orden exitosamente');
		$app->flash('type', 'success');
		$app->flashKeep();
	}else{
		$app->flash('message', 'porfavor seleccione una mesa');
		$app->flash('type', 'error');
		$app->flashKeep();
	}
	return $app->redirect($app->urlFor('load-user'));
})->name('ordenar-cosina');

$app->post('/cobrar/',function () use($app){
	$id_mesa = $_POST['id_mesa'];
	$st = $app->db->prepare("SELECT id, deuda, pedidos FROM mesas WHERE id = ?");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute(array($id_mesa));
	$datos_mesa = $st->fetch();
	$arr_id_pedidos = explode(",", $datos_mesa->pedidos);
	unset($arr_id_pedidos[0]);
	$platillos = array();
	$ingreso = $datos_mesa->deuda;
	foreach ($arr_id_pedidos as $id) {
		$st = $app->db->prepare("SELECT platillo FROM menu WHERE id = ?");
		$st->setFetchMode(PDO::FETCH_OBJ);
		$st->execute(array($id));
		$aux = $st->fetch();
		$platillos[] = $aux->platillo;
	}
	$cadena_platillos = implode(",", $platillos);
	$st = $app->db->prepare("UPDATE mesas SET status = 't', deuda = 0, pedidos = '', detalles= '', hora = null WHERE id = ?");
	$st->execute(array($id_mesa));
	$st = $app->db->prepare("INSERT INTO ingresos (pedido, id_mesa, ingreso, fecha, hora) VALUES(?, ?, ?, now(), now())");
	$st->execute(array($cadena_platillos, $id_mesa, $ingreso));
	return $app->redirect($app->urlFor('load-user'));
})->name('cobrar_mesa');
?>