<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/configuraciones.php';

$has_db = function ($app) {
  return function () use ($app) {
    if(is_null($app->db)){
      return "base de datos no encontrada";
    } else {
      return true;
    }
  };
};

$is_logged = function ($app) {
  return function () use ($app) {
    if(isset($app->session['usuario']) and !is_null($app->session['usuario'])){
      return true;
    } else {
      return $app->redirect($app->urlFor('home'));
    }
  };
};

$app->get('/',$has_db($app), function() use($app) {
    return $app->render('principal.twig');
})->name('home');

$app->post('/edit-user/',$has_db($app),function() use($app){
  $curpass = $_POST['curclave'];
  $newpass = $_POST['newclave'];
  $newpass2 = $_POST['newclavex2'];
  $usuario = ($_POST['usr'] == 1 || $_POST['usr'] == 2 || $_POST['usr'] == 3) ? $_POST['usr'] : "falsito";
  $st2 = $app->db->prepare("SELECT clave FROM usuarios WHERE id = ?");
  $st2->execute(array($usuario));
  $clave = $st2->fetch();
  $message = null;
  $type = null;
  if ($usuario != "falsito" && $newpass === $newpass2 && $clave['clave'] === md5($curpass)) {
  $st = $app->db->prepare("UPDATE usuarios SET clave = ? where id = ?");
  $st->execute(array(md5($newpass),$usuario));      
  $message = 'la contraseña se editó correctamente';
  $type = 'success';
  }else{
    $message = 'ha ocurrido un error';
    $type = 'error';
  }
    $app->flash('message', $message);
    $app->flash('type',$type);
    $app->flashKeep();
    return $app->redirect($app->urlFor('home'));
})->name('edit-user');

$app->post('/login/',$has_db($app),function() use($app){
  $_SESSION['usuario'] = null;
  $usuario = $_POST['usuario'];
  $clave = $_POST['clave'];
  $st =$app->db->prepare('SELECT clave,username FROM usuarios WHERE id = ?');
  $st->execute(array($usuario));
  $user = $st->fetch();
  if ($user['clave'] == md5($clave)) {
    $_SESSION['usuario'] = $user['username'];
    $app->redirect($app->urlFor('load-user'));
  }else{
    $app->flash('message', 'contraseña incorrecta');
    $app->flash('type','error');
    $app->flashKeep();
    return $app->redirect($app->urlFor('home'));
  }
})->name('login');

$app->get('/log-out',function() use($app){
  session_destroy();
  $app->redirect($app->urlFor('home'));
})->name('logout');

include_once CONTROLLERS_DIR.'users.php';
include_once CONTROLLERS_DIR.'pedidos.php';
include_once CONTROLLERS_DIR.'cocina.php';
include_once CONTROLLERS_DIR.'admin.php';
$app->run();
 ?>
