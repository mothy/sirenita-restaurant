CREATE DATABASE restaurant;
\c restaurant
CREATE TABLE usuarios(
	id INT NOT NULL PRIMARY KEY,
	username TEXT,
	clave VARCHAR(32)
);
CREATE TABLE menu(
	id SERIAL PRIMARY KEY,
	platillo TEXT,
	precio FLOAT,
	existencia BOOLEAN
);
CREATE TABLE mesas(
	id SERIAL PRIMARY KEY,
	numero VARCHAR(3),
	status boolean,
	deuda FLOAT,
	detalles TEXT,
	pedidos TEXT,
	hora TIME
);

CREATE TABLE carrito(
	mesa VARCHAR(3),
	pedidos TEXT
);
CREATE TABLE ingresos(
	id SERIAL,
	id_mesa INTEGER,
	pedido TEXT,
	ingreso FLOAT,
	fecha DATE,
	hora TIME,
);
SET DATESTYLE TO 'EUROPEAN';
-------INSERTS EN TABLA USUARIOS PARA GENERAR USUARIOS POR DEFAULT---------
ALTER TABLE mesas ADD COLUMN hora TIME;
INSERT INTO usuarios VALUES(1,'caja',md5('12345'));
INSERT INTO usuarios VALUES(2,'cocina',md5('12345'));
INSERT INTO usuarios VALUES(3,'admin',md5('12345'));

INSERT INTO menu (platillo,precio,existencia) VALUES('hamburguesa', 30.00, 't');
INSERT INTO menu (platillo,precio,existencia) VALUES('churros', 40.00, 't');

INSERT INTO mesas (numero,status,deuda) VALUES('1', 't',350.55);
---------INSERT INICIAL DEL CARRITO-----------------------
INSERT INTO carrito VALUES(0,'');