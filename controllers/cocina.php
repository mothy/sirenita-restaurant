<?php 

$app->post('/refreshkitchen/', function () use($app){
	$st = $app->db->prepare("SELECT numero, status, detalles, pedidos, hora FROM mesas ORDER BY(numero)");
	$st->setFetchMode(PDO::FETCH_OBJ);
	$st->execute();
	$mesas = $st->fetchAll();
	foreach ($mesas as $val) {
		$idp = explode(",", $val->pedidos);
		unset($idp[0]);
		$pedidos = array();
		foreach ($idp as $key) {
			$st = $app->db->prepare("SELECT platillo FROM menu WHERE id = ?");
			$st->setFetchMode(PDO::FETCH_OBJ);
			$st->execute(array($key));
			$aux = $st->fetch();
			$pedidos[] = $aux->platillo;
		}
		$val->pedidos = $pedidos;
	}
	$data['mesas'] = $mesas;
	//echo json_encode($data);
	return $app->render('usuarios/cocina-body.twig', $data);
})->name('refrescar-cocina');
 ?>